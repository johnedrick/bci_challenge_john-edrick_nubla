"""
Difficulty: easy
"""

from selenium import webdriver
from bs4 import BeautifulSoup
from pandas import read_csv


def translateToText(items):
	"""
	This function will remove the HTML tag and will only get the HTML content and/or HTML value
	"""
	filtered = []
	for item in items:
		if item.getText() == "Download":
			#	If the HTML content is equal to 'Download' don't append
			break
		else:
			if item.find('a'):
				#	If the item found a 'a' element tag append the 'href' attribute
				getHref = item.find('a').get('href')
				filtered.append(getHref)
			filtered.append(item.getText())
	return filtered


def main():
	
	data = read_csv("challenge_1_output.csv")
	description = data['description'].tolist() #converting column data to list
	url = 'https://www.emservices.com.sg/tenders/'
	chrome_browser = webdriver.Chrome('../chromedriver')
	chrome_browser.get(url)
	search_input =  chrome_browser.find_element_by_xpath("//*[@id='tnTable1_filter']/label/input") #	this xpath will locate the input type search

	output = []

	for x in description:
		# On every X in Description the browser will search for X and append the value to output	
		search_input.clear() # this clear the input search if there is a value presented
		search_input.send_keys(x.replace("â€“","-")) #	there is some character in csv that was presented as 'â€“' instead of '-' so we need to replace it first before we put it in the search input
		html = chrome_browser.page_source
		soup = BeautifulSoup(html,'html.parser')
		section =  soup.select('#tnTable1 > tbody > tr > td') #	with the use of CSS Selecter soup can easily select the element we will grab
		output.append(translateToText(section)) #	before appending the text we will pass it first in this function to remove the HTML Tag
	
	count = 0 
	for x in output:
		count += 1
		print("~"*20)
		print("Item #",(count))
		print("Advertisement Data: ",x[0])
		print("Closing Date: ",x[1])
		print("Client: ",x[2])
		print("Description: ",x[4])
		print("Eligibility: ",x[5])
		print("Link: ",x[3])

	chrome_browser.quit()

if __name__ == '__main__':
	main()
